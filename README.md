## О Keeperpass web
Сервис для сохранения паролей. В данном приложении реализован API для мобильных приложений IOS и Android

## Уязвимости безопасности
Если вы нашли уязвимости в безопастности просьба писать на e-mail [support@rackot.com](mailto:support@rackot.com)

## Лицензия
 [MIT license](https://opensource.org/licenses/MIT).
