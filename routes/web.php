<?php


use Illuminate\Http\Request;
use App\Http\Controllers\Auth\LoginController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['verify' => true]);//Включение подтверждения по электронной почте
Route::get('/verify/{token}', 'Auth\RegisterController@verify')->name('register.verify');

Auth::routes();
Route::get('auth/{provider}', [ LoginController::class, 'redirectToProvider' ]);
Route::get('auth/{provider}/callback', [ LoginController::class, 'handleProviderCallBack' ]);

Route::put('{id}/change-password', 'Auth\ChangePasswordController@changePassword')->name('pass');

Route::get('password/changepassword', function () {
    return view('auth.passwords.changepassword', ['name' => Auth::user()->name]);
});

Route::get('/', function () {
    return view('welcome');
});

Route::group(['namespace' => 'Web', 'as' => 'web.'], function () {

    Route::group(['namespace' => 'Category', 'as' => 'category.'], function () {
        Route::get('category/', 'CategoryController@index')->name('index');
    });
    Route::get('switch-lang/{langCode}', 'LocaleController@setLang')->name('set.lang') ;
    Route::get('send-email/{message}', 'LocaleController@contactMe')->name('send.email') ;
});

//Route::get('switch-lang/{langCode}', 'App\Http\Controllers\Web\LocaleController@setLang')->name('set.lang') ;

//Route::get('/categories', function () {
//    $categories = \App\Category::all();
//    return view('categories', compact('categories'));
//});


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/feedback', 'FeedbackController@index')->name('feedback.index');
Route::post('/feedback', 'FeedbackController@send')->name('feedback.send');
