<?php

use Illuminate\Http\Request;
use App\Http\Controllers\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware("localization")->group(function () {
    Route::post('auth/provider/', [LoginController::class, 'authToProviderAPI']);//google/{serverAuthCode}
    Route::post('register/', 'Auth\RegisterController@register_api');

    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });

//Route::post('/logout', 'LoginController@logout');

    Route::group(['namespace' => 'Api', 'as' => 'api.'], function () {

        Route::group(['namespace' => 'Auth', 'as' => 'auth.'], function () {
            Route::post('changepassword/', 'ChangePasswordController@changePassword');
        });

        Route::group(['namespace' => 'Logout', 'as' => 'logout.'], function () {
            Route::post('logout/', 'LogoutController@logout');
        });

        Route::group(['namespace' => 'Category', 'as' => 'category.'], function () {
            Route::post('category/{id}', 'CategoryController@store');
            Route::get('category/', 'CategoryController@index');
            Route::get('category/{id}', 'CategoryController@show');
            Route::put('category/{id}', 'CategoryController@update');
            Route::delete('category/{id}', 'CategoryController@delete');
        });

        Route::group(['namespace' => 'Password', 'as' => 'password.'], function () {
            Route::post('password/{id}', 'PasswordController@store');
            Route::get('password/', 'PasswordController@index');
            Route::get('password/{id}', 'PasswordController@show');
            Route::put('password/{id}', 'PasswordController@update');
            Route::delete('password/{id}', 'PasswordController@delete');
        });
    });
});
