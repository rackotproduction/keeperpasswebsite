<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $fillable = [ 'category_name', 'category_note', 'user_id'];

    // Пользователь
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
