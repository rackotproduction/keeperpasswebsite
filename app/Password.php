<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Password extends Model
{
    protected $table = 'passwords';
    protected $fillable = [ 'category_id', 'login', 'password', 'password_note', 'user_id'];

    // Пользователь
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
