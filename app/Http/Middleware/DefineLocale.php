<?php

namespace App\Http\Middleware;

use App\Helpers\LanguageHelper;
use Closure;
use Illuminate\Support\Facades\Session;

class DefineLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!empty($lang = Session::get('lang'))) {
            if (in_array($lang, LanguageHelper::AVAILABLE_LANGUAGES)) {
                \App::setLocale($lang);
            }
        }
        return $next($request);
    }
}
