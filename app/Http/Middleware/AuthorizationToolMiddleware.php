<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class AuthorizationToolMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //для IOS перезаписываю токен из X-Access-Token в Authorization
        if ($request->headers->get('X-Access-Token') != '')
        {
            $request->headers->set('Authorization', $request->headers->get('X-Access-Token'));
        }


        return $next($request);
    }
}
