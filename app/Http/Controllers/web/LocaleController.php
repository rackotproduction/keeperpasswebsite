<?php

namespace App\Http\Controllers\Web;

use App\Helpers\LanguageHelper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class LocaleController extends Controller
{
    public function setLang(string $langCode)
    {
        if (in_array($langCode, LanguageHelper::AVAILABLE_LANGUAGES)) {
            \Session::put('lang', $langCode);
        }
        return redirect()->back();
    }
}
