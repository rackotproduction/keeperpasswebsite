<?php

namespace App\Http\Controllers\Api\Category;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        $user_id = Auth::user()->id;
        return Category::where('user_id', '=', $user_id)->orderBy('id')->get();
    }

    public function show($id)
    {
        $user_id = Auth::user()->id;
        return Category::where('id', '=', $id)->where('user_id', '=', $user_id)->first();//where('id', '=', $id);//find($id);
    }

    public function store(Request $request)
    {
        $Category = Category::create(
            [
                'category_name' => $request['category_name'],
                'category_note' => $request['category_note'],
                'user_id' => Auth::user()->id
            ]
        );

        return $Category;//Category::all();
    }

    public function update(Request $request, $id)
    {
        $user_id = Auth::user()->id;
        $Category = Category::where('user_id', '=', $user_id)->where('id', '=', $id)->first();//Category::findOrFail($id);
        $Category->update($request->all());

        return $Category;
    }

    public function delete(Request $request, $id)
    {
        $Category = Category::where('user_id', '=', Auth::user()->id)->where('id', '=', $id)->first();//Category::findOrFail($id);
        $Category->delete();
        $user_id = Auth::user()->id;

        return 204;
    }
}
