<?php

namespace App\Http\Controllers\Api\Logout;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use  Illuminate\Auth\RequestGuard;

class LogoutController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    protected function guard()
    {
        return Auth::guard('api');
    }

    public function logout(Request $request)
    {
        if (!$this->guard()->check()) {
            return response([
                'message' => __('login.errors.not_found_session')
            ], 404);
        }

        // Taken from: https://laracasts.com/discuss/channels/laravel/laravel-53-passport-password-grant-logout
        $request->user('api')->token()->revoke();

//        $request->user('api')->token()->delete(); Временно не нужено
//        Auth::guard()->logout();
//        Session::flush();
//        Session::regenerate();

        return response([
            'message' => __('login.errors.logged')
        ]);
    }
}
