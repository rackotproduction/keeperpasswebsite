<?php

namespace App\Http\Controllers\Api\Auth;

use App\User;
use phpseclib\Crypt\RSA;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use League\OAuth2\Server\Exception\OAuthServerException;
use Psr\Http\Message\ServerRequestInterface;
use \Laravel\Passport\Http\Controllers\AccessTokenController as ATC;

class AccessTokenController extends ATC {

    public function issueToken(ServerRequestInterface $request){
        try {
            // Fetching username from request
            $username = $request->getParsedBody()['username'];
            // Fetching the User
            $user = User::where('email', '=', $username)->first();
            if ($user->status !== User::STATUS_ACTIVE) {
                throw new \Exception(__('register.errors.accept_account'));
            }
            // Genereting token
            $tokenResponse = parent::issueToken($request);
            //convert response to json string
            $content = $tokenResponse->content();
//            $content->email = $user->email;
            //convert json to array
            $data = json_decode($content, true);
            return $data;

        } catch (OAuthServerException $e) {
            return ['Message' => 'The suer credentials were incorrect!'];
        }catch (\Exception $e) {
            return ['Message' => $e->getMessage()];
        }
    }
}
