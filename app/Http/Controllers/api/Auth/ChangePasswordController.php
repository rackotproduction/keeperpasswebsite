<?php

namespace App\Http\Controllers\Api\Auth;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Auth\RequestGuard;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    protected function guard()
    {
        return Auth::guard('api');
    }

    public function changePassword(Request $request)
    {


        if (!$this->guard()->check()) {
            return response([
                    'message' => __('resetpassword.errors.user_not_found')
            ], 404);
        }

        $user = $request->user('api');

        if ($request->password != $request->password_confirmation) {
            return response([
                'message' => __('resetpassword.errors.not_validate_pass')
            ], 404);
        }

        $request->validate([
            'old_password' => 'required',
            'password' => 'required|confirmed'
        ]);

        if (!Hash::check($request->old_password, $user->password)) {
            return response([
                'message' => __('resetpassword.errors.password_incorrect')
            ], 404);
        }

        $user->update([
            'password' => Hash::make($request->password)
        ]);

        return response([
            'successful' => true
            ,'message' => __('resetpassword.errors.change_pass_successful')
        ]);
    }
}
