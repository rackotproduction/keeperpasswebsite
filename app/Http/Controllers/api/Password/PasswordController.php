<?php

namespace App\Http\Controllers\Api\Password;

use App\Password;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index($category_id)
    {
        $user_id = Auth::user()->id;
        return [];//Password::where('user_id', '=', $user_id)->where('category_id', '=', $category_id)->orderBy('id')->get();
    }

    public function show($category_id)
    {
        $user_id = Auth::user()->id;
        return Password::where('category_id', '=', $category_id)->where('user_id', '=', $user_id)->get();
    }

    public function store(Request $request)
    {
        $Password = Password::create(
            [
                'category_id' => $request['category_id'],
                'login' => $request['login'],
                'password' => $request['password'],
                'password_note' => $request['password_note'],
                'user_id' => Auth::user()->id
            ]
        );

        return $Password;
    }

    public function update(Request $request, $id)
    {
        $user_id = Auth::user()->id;
        $Password = Password::where('user_id', '=', $user_id)->where('id', '=', $id)->first();
        $Password->update($request->all());

        return $Password;
    }

    public function delete(Request $request, $id)
    {
        $Password = Password::where('user_id', '=', Auth::user()->id)->where('id', '=', $id)->first();
        $Password->delete();
        $Password->delete();
        $user_id = Auth::user()->id;

        return 204;
    }
}
