<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Two\GoogleProvider;
use \Laravel\Passport\Client;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authToProviderAPI(Request $request)
    {
        try {
            //Provider is 'google' here
            $driver = Socialite::driver($request['provider']);
            //Запрашиваем токен из Google по $serverAuthCode полученный от стороннего приложения
            $access_token11 = $driver->getAccessTokenResponse($request['ServerAuthCode'])['access_token'];
            //По токену получаем User из Google
            $user = $driver->userFromToken($access_token11);
            //Поиск существующего пользователя или его создание
            $authUser = $this->findOrCreateUser($user, $request['provider']);
            //Авторизовываем пользователя
            Auth::login($authUser, true);
            //Получаем токен для API
            $authToken = \Auth::user()->createToken('authToken');
            //print "accessToken: " . $authToken->accessToken;
            //формируем json по подобию
            $ReturnArrayToken = array('token_type' => 'Bearer', 'access_token' => $authToken->accessToken);
            return $ReturnArrayToken;
        } catch (\Exception $ex) {
            $ErrorMEssage = $ex->getMessage();
            return ['error' => 'invalid_grant', 'error_message' => $ErrorMEssage];
        }
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->stateless()->user();
        $authUser = $this->findOrCreateUser($user, $provider);
        if ($authUser == null){
            return redirect()->route('login')
                ->withErrors(['email' => __('register.errors.google_denied')]);
        }

        Auth::login($authUser, true);
        return redirect($this->redirectTo);
//        Laravel\Passport\Http\Controllers\AccessTokenController@issueToken
    }

    public function findOrCreateUser($user, $provider)
    {
        $authUser = User::where('provider_id', $user->id)->first();
        if ($authUser)
        {
            return $authUser;
        }
        if (User::where('email', $user->email)->exists()) {
            return null;
        }
        //, потому что уже ранее были зарегестрированные на данный email адрес. Пожалуйста воспользуйтесь логином и паролем от вашей учётной записи.

        return User::create([
            'name' => $user->name,
            'email'=> $user->email,
            'password' => '',
            'provider'=> strtoupper($provider),
            'provider_id'=> $user->id,
            'status' => 10,
        ]);
    }

    public function authenticated(Request $request, $user)
    {
        if ($user->status !== User::STATUS_ACTIVE) {
            $this->guard()->logout();
            return back()->withErrors(['verify' => __('register.errors.accept_account')]);
        }
        return redirect()->intended($this->redirectPath());
    }
}
