<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;

class ChangePasswordController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    protected $redirectTo = RouteServiceProvider::HOME;

    public function changePassword(Request $request, $id)
    {
        $user = auth()->user();

        $request->validate([
            'old_password' => 'required',
            'password' => 'required|confirmed'
        ]);

        if (!Hash::check($request->old_password, $user->password)) {
            return back()
                ->with('error', __('resetpassword.errors.not_found_pass_db'));
        }

        $user->update([
            'password' => Hash::make($request->password)
        ]);

        return redirect()
            ->route('home')
            ->with('message', __('resetpassword.errors.accept_change_password'));

    }
}
