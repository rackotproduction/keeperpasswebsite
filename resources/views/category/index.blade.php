@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Категории</div>
                    <div class="panel-body">
                        <table class="table table-striped table-hover">
                            <tr>
                                <th>{{trans('category.user')}}</th>
                                <th>{{trans('category.category_name')}}</th>
                                <th>{{trans('category.note')}}</th>
                                <th>{{trans('category.date')}}</th>
                            </tr>

                            @foreach ($categories as $category)
                                <tr>
                                    <td>{{ $category->user ? $category->user->name : 'none' }}</td>
                                    <td>{{ $category->category_name }}</td>
                                    <td>{{ $category->category_note }}</td>
                                    <td>{{ $category->created_at->diffForHumans() }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
