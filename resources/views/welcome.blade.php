<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="google-site-verification" content="OVu9aeSVgqcQJCRiZyMmonX1mQqLhpgkNzBC3m7-k44" />
    <title>Keeperpass by Rackot production</title>
    <!-- Bootstrap core CSS -->
{{--    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">--}}
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Custom fonts for this template -->
{{--    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">--}}
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="css/simple-line-icons.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
    <!-- Plugin CSS -->
    <link rel="stylesheet" href="device-mockups/device-mockups.min.css">
    <!-- Custom styles for this template -->
    <link href="css/new-age.min.css?" rel="stylesheet">
</head>

<body id="page-top">

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">Keeperpass</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            {{ __('welcome.menu') }}
            <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="/login">{{ __('welcome.login') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="/register">{{ __('welcome.register') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#download">{{ __('welcome.download') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#features">{{ __('welcome.about') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#contact">{{ __('welcome.contacts') }}</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<header class="masthead">
    <div class="container h-100">
        <div class="switch-lang" style="max-width: 5em;min-height: 2em;margin-left: auto;margin-right: 0em;">
            <a href="{{route('web.set.lang', ['langCode' => 'ru']) }}">
                <img width="32" src="https://www.ph4.ru/DL/ICO/flags/classic_go-flat_64/ru.png"
                     alt="">
            </a>
            <a href="{{route('web.set.lang', ['langCode' => 'en']) }}">
                <img width="32" src="https://www.ph4.ru/DL/ICO/flags/classic_go-flat_64/uk.png"
                     alt="">
            </a>
        </div>
        <div class="row h-100">
            <div class="col-lg-7 my-auto">
                <div class="header-content mx-auto">
                    <h1 class="mb-5">{{ __('welcome.slide1_title') }}</h1>
                    <a href="#download" class="btn btn-outline btn-xl js-scroll-trigger">{{ __('welcome.slide1_download_button') }}</a>
                </div>
            </div>
            <div class="col-lg-5 my-auto">
                <div class="device-container">
                    <div class="device-mockup iphone6_plus portrait white">
                        <div class="device">
                            <div class="screen">
                                <!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! -->
                                <img src="images/demo-screen-1.jpg" alt="">
                            </div>
                            <div class="button">
                                <!-- You can hook the "home button" to some JavaScript events or just remove it -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<section class="download bg-primary text-center" id="download">
    <div class="container">
        <div class="row">
            <div class="col-md-8 mx-auto">
                <h2 class="section-heading">{{ __('welcome.slide2_title') }}</h2>
                <p>{{ __('welcome.slide2_description') }}</p>
                <div class="badges">
                    <a class="badge-link" target="_blank" href="https://play.google.com/store/apps/details?id=com.rackot.ikolganov.keeperpass"><img src="images/google-play-badge.svg" alt=""></a>
                    <a class="badge-link" href="#"><img src="images/app-store-badge.svg" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="features" id="features">
    <div class="container">
        <div class="section-heading text-center">
            <h2 class="section-heading">{{ __('welcome.slide3_title') }}</h2>
            <p class="text-muted">{{ __('welcome.slide3_description') }}</p>
            <hr>
        </div>
        <div class="row">
            <div class="col-lg-4 my-auto">
                <div class="device-container">
                    <div class="device-mockup iphone6_plus portrait white">
                        <div class="device">
                            <div class="screen">
                                <!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! -->
                                <img src="images/demo-screen-1.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="button">
                                <!-- You can hook the "home button" to some JavaScript events or just remove it -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 my-auto">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="feature-item">
                                <i class="icon-screen-smartphone text-primary"></i>
                                <h3>Все виды устройств</h3>
                                <p class="text-muted">Данное приложение распростроняется для Windows IOS и Android.</p>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="feature-item">
                                <i class="icon-camera text-primary"></i>
                                <h3>Ещё не придумал</h3>
                                <p class="text-muted">Но вы определённо можете себя фотографировать, когда и где угодно!</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="feature-item">
                                <i class="icon-present text-primary"></i>
                                <h3>Бесплатное использование</h3>
                                <p class="text-muted">Данное приложение можно бесплатно скачать и использовать!</p>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="feature-item">
                                <i class="icon-lock-open text-primary"></i>
                                <h3>Удобная авторизация</h3>
                                <p class="text-muted">Для авторизации вы можете использовать различные популярные сервисы!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<section class="contact bg-primary" id="contact">
    <div class="container">
        <h2 class="section-heading">{{ __('welcome.slide4_title') }}
            <i class="fa fa-heart"></i></h2>
        <ul class="list-inline list-social">
            <li class="list-inline-item social-twitter">
                <a href="#">
                    <i class="fa fa-twitter"></i>
                </a>
            </li>
            <li class="list-inline-item social-facebook">
                <a href="#">
                    <i class="fa fa-facebook-f"></i>
                </a>
            </li>
            <li class="list-inline-item social-google-plus">
                <a href="#">
{{--                    <i class="fa google-plus-g"></i>--}}
                    <i class="fa fa-google-plus"></i>
                </a>
            </li>
        </ul>
    </div>
</section>

<footer>
    <div class="container">
        <p>&copy; Copyright 2013 - {{date('Y')}} Rackot production. All Rights Reserved.</p>
        <ul class="list-inline">
            <li class="list-inline-item">
{{--                <a href="#">Privacy</a>--}}
            </li>
            <li class="list-inline-item">
{{--                <a href="#">Terms</a>--}}
            </li>
            <li class="list-inline-item">
{{--                <a href="#">FAQ</a>--}}
            </li>
        </ul>
    </div>
</footer>

<!-- Bootstrap core JavaScript -->
<script
    src="https://code.jquery.com/jquery-3.4.1.js"
    integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
    crossorigin="anonymous"></script>
{{--<script src="vendor/jquery/jquery.min.js"></script>--}}
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
{{--<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>--}}
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>

<!-- Plugin JavaScript -->
<script src="js/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for this template -->
<script src="js/new-age.min.js"></script>

</body>

</html>
