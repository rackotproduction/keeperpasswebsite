@component('mail::message')
# Подтверждение email

Пожалуйста, перейдите по следующей ссылке:

@component('mail::button', ['url' => route('register.verify', ['token' => $user->verify_token])])
Подтвердить email
@endcomponent
<br><br>
Спасибо, за то, что с нами.<br>
Команда Rackot production<br>
{{ config('app.name') }}
@endcomponent
