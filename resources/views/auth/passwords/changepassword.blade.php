@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><a href="https://keeperpass.rackot.ru/home">Назад</a> | Изменение пароля для аккаунта {{ $name }}</div>

                <div class="card-body">
                    @if ( session()->has('message') )
                        <div class="alert alert-success alert-dismissible">
                            {{ session()->get('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    @if ( session()->has('error') )
                        <div class="alert alert-danger alert-dismissible">
                            {{ session()->get('error') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    @if ( $errors->any() )
                        <div class="alert alert-danger">
                            <ul class="edit-profile">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form method="POST" action="{{ route('pass', Auth::user() ) }}" class="mb-3">
                        @csrf
                        @method("PUT")
                        <div class="form-group">
                            <label for="old_password">Старый пароль</label>
                            <input type="password" name="old_password" class="form-control" id="old_password">
                        </div>
                        <div class="form-group">
                            <label for="password">Новый пароль</label>
                            <input type="password" name="password" class="form-control" id="password">
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">Подтвердите пароль</label>
                            <input type="password" name="password_confirmation"
                                   class="form-control" id="password_confirmation">
                        </div>
                        <button type="submit" class="btn btn-primary">Изменить пароль</button>
                    </form>

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
