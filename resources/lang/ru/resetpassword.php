<?php

return [
    'title' => 'Сброс пароля',
    'email' => 'E-Mail адрес',
    'btn_rest_password' => 'Отправить ссылку для сброса пароля',

    'errors' => [
        'not_found_pass_db' => 'Пароль не найден в базе.',
        'accept_change_password' => 'Успешно обновлено!',
        'user_not_found' => 'Пользователь не найден.',
        'not_validate_pass' => 'Пароли не совпадают.',
        'password_incorrect' => 'Неверный пароль, пароль не найден в базе данных.',
        'change_pass_successful' => 'Пароль успешно изменён.',
    ]
];
