<?php

return [
    'title' => 'Авторизация',
    'email' => 'Email адрес',
    'password' => 'Пароль',
    'remember' => 'Запомнить',
    'btn_login' => 'Войти',
    'btn_login_with_google' => 'Войти через google',
    'lnk_lostpassword' => 'Забыли пароль?',

    'errors' => [
        'logged' => 'Вы успешно вышели из системы.',
        'not_found_session' => 'Активный сеанс пользователя не найден.',
        'incorrect_log_or_pass' => 'Неправильный логин или пароль.',
    ]
];
