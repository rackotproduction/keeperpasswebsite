<?php

return [
    'menu' => 'Menu',
    'login' => 'LOGIN',
    'register' => 'REGISTER',
    'download' => 'DOWNLOAD',
    'about' => 'ABOUT',
    'contacts' => 'CONTACTS',
    'slide1_title' => 'New solution for saving passwords!',
    'slide1_download_button' => 'Download now for free!',
    'slide2_title' => 'Find out more!',
    'slide2_description' => 'Our app is available on any mobile device! Download now to get started!',
    'slide3_title' => 'Functions',//'Unlimited Possibilities, Unlimited Fun',
    'slide3_description' => 'Check out what you can do with this app!',
    'slide4_title' => 'We are always glad to see you!',
];
