<?php

return [
    'title' => 'Login',
    'email' => 'E-Mail address',
    'password' => 'Password',
    'remember' => 'Remember',
    'btn_login' => 'Login',
    'btn_login_with_google' => 'Login with google',
    'lnk_lostpassword' => 'Forgot your password?',

    'errors' => [
        'logged' => 'You have successfully logged out!',
        'not_found_session' => 'No active user session was found.',
        'incorrect_log_or_pass' => 'The suer credentials were incorrect!',
    ]
];
