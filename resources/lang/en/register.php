<?php

return [
    'title' => 'Register',
    'name' => 'Name',
    'email' => 'E-Mail Address',
    'password' => 'Password',
    'confirm_password' => 'Confirm Password',
    'btn_register' => 'Register',

    'errors' => [
        'accept_account' => 'You need to verify your account. Please check your email.',
        'google_denied' => 'You cannot use Google authorization. '.
            'An account has already been registered to this email address. '.
            'Please use your username and password for your account.',
        'email_accepted' => 'Your email address has been verified.',
        'url_cannot_be_identified' => 'Your link could not be identified.',
        'check_email_for_identified' => 'Check your email and click on the link to verify your email address.',
        ]
];
