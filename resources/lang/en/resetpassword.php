<?php

return [
    'title' => 'Reset Password',
    'email' => 'E-Mail Address',
    'btn_rest_password' => 'Send Password Reset Link',

    'errors' => [
        'not_found_pass_db' => 'Password not found in database.',
        'accept_change_password' => 'Successfully updated!',
        'user_not_found' => 'User is not found',
        'not_validate_pass' => 'Password mismatch.',
        'password_incorrect' => 'Invalid password, password not found in the database.',
        'change_pass_successful' => 'Password changed successfully.',
    ]
];
