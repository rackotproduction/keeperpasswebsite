<?php

return [
    'body' => 'At the moment, the personal account is under development, please use the mobile application.',
    'title' => 'My account',
    'change_password' => 'Change password',
];
